<?php
/**
 * Customizations for the WordPress admin
 */


// Dashboard: Hide the WordPress Welcome panel
function hide_welcome_panel($null, $object_id, $meta_key, $single) {
	if ($meta_key == 'hide_welcome_panel') { return 0; }
}
add_filter('get_user_metadata', 'hide_welcome_panel', 1, 4);


// Dashboard: Remove WordPress default dashboard widgets
function remove_dashboard_widgets() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_widgets' );


// Dashboard: Move WP Help widget to the right
function position_dashboard_widgets() {
        global $wp_meta_boxes;
        $widget = $wp_meta_boxes['dashboard']['normal']['core']['cws-wp-help-dashboard-widget'];
        unset( $wp_meta_boxes['dashboard']['normal']['core']['cws-wp-help-dashboard-widget'] );
        $wp_meta_boxes['dashboard']['side']['core']['cws-wp-help-dashboard-widget'] = $widget;
}
add_action( 'wp_dashboard_setup', 'position_dashboard_widgets', 20);


// Menu: Hide functions from regular admins
function hide_menu_items() {
	if (is_admin() && !is_network_admin() && !is_super_admin() && !current_user_can('admin_config_edit')) { // name of custom capability
		// TODO modify menu
	}
}

/*
 * Shortcode to limit conent to administrators
 * https://stackoverflow.com/questions/39593273/user-roles-shortcode
 * defaults to Administrator, but one may specificy a list of 
 * roles with a comma separated string.  
 * 
 * To use the shortcode, wrap content with: 
 *      [admin_content][/admin_content]
 * To include diffrent roles OR additional roles to limit content, specify the
 * roles by including them in the shortcode:
 *      [admin_content roles="administrator, editor"][/admin_content]
 */
function filter_content_by_role( $atts, $content = null ) 
{
    shortcode_atts(array('roles'=> array('administrator')), $atts);

    $roles = $atts['roles'];
    if (is_string($roles)){
        $roles = explode(',', $roles);
    }

    foreach ($roles as $role) {
       if( current_user_can($role) ){
           return $content;
       }
    }
}
add_shortcode( 'admin_content', 'filter_content_by_role' );

?>