<?php
/** =============================================================================
*                          Formidable Forms custom hooks
*  =============================================================================
*/


/**
* Show substring of characters in form data view
*/
add_filter('frmpro_fields_replace_shortcodes', 'frm_substr_shortcode', 10, 4);
function frm_substr_shortcode($replace_with, $tag, $atts, $field){
    if(isset($atts['substr'])){
        $args = split(',', $atts['substr']);
        $replace_with = substr($replace_with, $args[0], $args[1]);
    }
    return $replace_with;
}


/**
* Show start of characters in form data view
*/
add_filter('frmpro_fields_replace_shortcodes', 'frm_excerpt_shortcode', 10, 4);
function frm_excerpt_shortcode($replace_with, $tag, $atts, $field){
    if(isset($atts['excerpt'])){
        $length = ($atts['excerpt'] > 1) ? $atts['excerpt'] : 30;
        foreach((array)$replace_with as $v){
            $new .= substr($v, 0, $length);
            $new .= (strlen($v) > $length) ? '…' : '';
        }
        return $new;
    }
    return $replace_with;
}



/**
* Remove text inside brackets in form data view
*
add_filter('frmpro_fields_replace_shortcodes', 'frm_removebrackets_shortcode', 10, 4);
function frm_removebrackets_shortcode($replace_with, $tag, $atts, $field){
    if(isset($atts['removebrackets'])){
        $replace_with = strstr($replace_with, '[', true);
    }
    return $replace_with;
} 
*/


/** 
* Hide fields when creating (show only when editing)
*/
add_filter('frm_field_type', 'change_my_field_type', 10, 2);
function change_my_field_type($type, $field){
    // Vendor Quality Report Problem: 238=ID, 260=Active, 261=Status, 262=Review Comments
    // , 280=Severity Rank
    if(in_array($field->id, array(238, 260, 261, 262))){ //ids of the fields to hide
        global $frm_vars;
        if((!is_admin() || defined('DOING_AJAX')) && (!isset($frm_vars['editing_entry']) || !$frm_vars['editing_entry'])) {
            //if not in admin area and not editing an entry
            $type = 'hidden'; //hide this field
        }
    }
    return $type;
}


/*
 * Only send email if status is set to "needs clarification"
 * 
 * $form_id == 21 => Vendor Quality Report Form
 * $args['email_key'] == 4881 => Email action ID 
 * $_POST['item_meta'][251][...][261] => 261 is id for "Status" field
 */
add_filter('frm_to_email', 'stop_the_email', 10, 4);
function stop_the_email($recipients, $values, $form_id, $args){
    if ( ($form_id == 21) && ($args['email_key'] == 4881) ) {
        if (isset( $_POST['item_meta'][251] )) {
            foreach ($_POST['item_meta'][251] as $problem_section) {
                if (isset( $problem_section[261] ) && $problem_section[261] == 'Needs Clarification') {
                    return $recipients;
                }
            }
			return array();
        }
    }
	return $recipients;
}

/** 
* Restrict options in dynamic fields when creating entries to include only options with field Active = Yes
*/
add_filter('frm_setup_new_fields_vars', 'filter_dfe_options_create', 25, 2);
function filter_dfe_options_create($values, $field){
    // Journal dynamic field
    // IDs of the dynamic fields
    // Vendor Quality Report Problem: 241=Journal
    // Final Quality Checklist: 150=Journal
    // CATS Academic Editor Account Request: 126=Journal
    if ( in_array($field->id, array(241, 150, 126)) && !empty( $values['options'] ) ) {
        $temp = $values;
        // id of the field (in linked form) that you want to filter by
        // Journals: 132=Active
        $temp['form_select'] = 132;
        $field2_opts = FrmProDynamicFieldsController::get_independent_options( $temp, $field );
        foreach ( $values['options'] as $id => $v ) {
            if ( isset( $field2_opts[$id] ) && ( $v == '' || $field2_opts[$id] == 'Yes'  ) ) {//Only include values where filtering field equals Yes
                continue;
            }
            unset( $values['options'][$id] );
        }
    }
    // Vendor dynamic field
    // IDs of the dynamic fields
    // Vendor Quality Report Problem: 246=Vendor
    if ( $field->id == 246 && !empty( $values['options'] ) ) {
        $temp = $values;
        // id of the field (in linked form) that you want to filter by
        // Vendors: 136=Active
        $temp['form_select'] = 136;
        $field2_opts = FrmProDynamicFieldsController::get_independent_options( $temp, $field );
        foreach ( $values['options'] as $id => $v ) {
            if ( isset( $field2_opts[$id] ) && ( $v == '' || $field2_opts[$id] == 'Yes' ) ) {//Only include values where filtering field equals Yes
                continue;
            }
            unset( $values['options'][$id] );
        }
    }
    // Problem Catalogue Type of Problem dynamic field
    // IDs of the dynamic fields
    // Vendor Quality Report Problem: 236=Type of problem
    if ( $field->id == 236 && !empty( $values['options'] ) ) {
        $temp = $values;
        // id of the field (in linked form) that you want to filter by
        // Problem Catalogue: 291=Active
        $temp['form_select'] = 291;
        // Didn't work as intended to show non-active entries when editing, so added check for user_can
        $field2_opts = FrmProDynamicFieldsController::get_independent_options( $temp, $field );
        foreach ( $values['options'] as $id => $v ) {
            if ( isset( $field2_opts[$id] ) && ( $v == '' || $field2_opts[$id] == 'Yes' || current_user_can( 'vendor_quality_team' ) ) ) {//Only include values where filtering field equals Yes
                continue;
            }
            unset( $values['options'][$id] );
        }
    }
    return $values;
}


/**
* Copy text from dynamic field
*/
/*
add_filter('frm_validate_field_entry', 'copy_my_dynamic_field', 10, 3);
function copy_my_dynamic_field( $errors, $posted_field, $posted_value ) {
    if ( $posted_field->id == 280 ) {
        $_POST['item_meta'][ $posted_field->id ] = FrmProEntriesController::get_field_value_shortcode( array( 'field_id' => 271, 'entry' => $_POST['item_meta'][236] ) );
    }
    return $errors;
}
*/

/**
* Copy fields from selected catalogue entry into problem report
*/
add_filter('frm_validate_field_entry', 'set_custom_repeating_val', 10, 4);
function set_custom_repeating_val($errors, $posted_field, $posted_value, $args){
// if problem report Active field is 'Open' (don't want to change data on closed reports)
    // if ($_POST['item_meta'][ 260 ] == 'Open') {
        // within the repeating section
        if ( $posted_field->id == 251 ) {
            foreach ( $_POST['item_meta'][ $posted_field->id ] as $row_num => $section_vals ) {
                if ( $row_num === 'form' ) {
                    continue;
                }
                if (empty($_POST['item_meta'][ $posted_field->id ][ $row_num ][236])) {
                    continue;
                }
                // populate Severity by copying from the selected problem entry from the catalogue
                $_POST['item_meta'][ $posted_field->id ][ $row_num ][ 280 ] = 
                FrmProEntriesController::get_field_value_shortcode( array( 'field_id' => 270, 'entry' => $_POST['item_meta'][ $posted_field->id ][ $row_num ][236] ) );
                // populate Severity Rank by copying from the selected problem entry from the catalogue
                $_POST['item_meta'][ $posted_field->id ][ $row_num ][ 283 ] = 
                FrmProEntriesController::get_field_value_shortcode( array( 'field_id' => 271, 'entry' => $_POST['item_meta'][ $posted_field->id ][ $row_num ][236] ) );
            }
        }
    // }
    return $errors;
}

/*
* Make required either: "volume & issue" OR Article IDs
*/
add_filter('frm_validate_entry', 'custom_errors', 10, 2);
function custom_errors($errors, $values) {
    if ($values['form_id'] == 21) { 
        $volume = $_POST['item_meta'][242]; //242 == Volume
        $issue = $_POST['item_meta'][243]; //243 == Issue
        $article_ids = $_POST['item_meta'][244]; //244 == Article IDs
               
        switch (TRUE) {
            case (!$volume && !$issue && !$article_ids):
                foreach (array(242,243,244) as $field_id) {
                    $errors['field'. $field_id] = 'Either Volume & Issue or Article IDs is required ';
                }
                break;
            case (!$volume && $issue):
                $errors['field'. 242] = 'Volume needed if Issue is included';
                    break;
            case ($volume && !$issue):
                $errors['field'. 243] = 'Issue needed if Volume is included';
                break;
            default:
                break;
        }

    }
    return $errors;
}
    
/*************************************************************
* Formidable User Registration
*/

// Reset the default user ID in the user ID field
add_filter('frm_get_default_value', 'reset_user_id', 10, 2);
function reset_user_id($new_value, $field){
    if ( in_array( $field->id, array( 290 ) ) ) { // the userID field
        $new_value = '0';
    }
    return $new_value;
}

?>